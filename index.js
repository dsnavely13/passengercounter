//initialize count as 0
//listen for clicks on increment button
//increment count var when button is clicked
// change count-el in html to reflect new count
let count = 0
let countEl = document.getElementById("count-el")
let saveEl=document.getElementById("save-el")

function increment() {
    count++
    countEl.textContent = count
}

function save() {
    saveEl.textContent += count + " - "
    count = 0
    countEl.textContent = count
}